from pysph.sph.equation import Equation, Group
from math import exp, pi, sin, cos
import numpy as np


class SetPressureBC(Equation):
    def initialize(self, d_idx, d_p):
        d_p[d_idx] = 0.0


class SetNoSlipWallVelocity(Equation):
    def initialize(self, d_idx, d_uf, d_vf, d_wf):
        d_uf[d_idx] = 0.0
        d_vf[d_idx] = 0.0
        d_wf[d_idx] = 0.0


class SetSlipWallVelocity(Equation):
    def initialize(self, d_idx, d_ug_star, d_vg_star, d_wg_star):
        d_ug_star[d_idx] = 0.0
        d_vg_star[d_idx] = 0.0
        d_wg_star[d_idx] = 0.0


class SetValuesonSolid(Equation):
    def initialize(self, d_idx, d_au):
        d_au[d_idx] = 0.0


class AddMomentumSourceTerm(Equation):
    def initialize(self, d_au, d_av, d_aw, d_idx):
        d_au[d_idx] = 0.0
        d_av[d_idx] = 0.0
        d_aw[d_idx] = 0.0


class AddContinuitySourceTerm(Equation):
    def initialize(self, d_arho, d_idx):
        d_arho[d_idx] = 0.0


class AddPressureEvolutionSourceTerm(Equation):
    def initialize(self, d_ap, d_idx):
        d_ap[d_idx] = 0.0


class SetValuesonSolidEDAC(Equation):
    def initialize(self, d_idx, d_u, d_v, d_w):
        d_u[d_idx] = 0.0
        d_v[d_idx] = 0.0
        d_w[d_idx] = 0.0


class XSPHCorrectionDummy(Equation):
    def initialize(self, d_idx, d_ax, d_ay, d_az):
        d_ax[d_idx] = 0.0
        d_ay[d_idx] = 0.0
        d_az[d_idx] = 0.0

    def post_loop(self, d_idx, d_ax, d_ay, d_az, d_u, d_v, d_w):
        d_ax[d_idx] = d_u[d_idx]
        d_ay[d_idx] = d_v[d_idx]
        d_az[d_idx] = d_w[d_idx]


def get_props(x, y, z, t, c0, mms='mms1'):
    if mms == 'mms1':
        from mms1 import get_props
        return get_props(x, y, z, t, c0)
    elif mms == 'mms2':
        from mms2 import get_props
        return get_props(x, y, z, t, c0)
    elif mms == 'mms3':
        from mms3 import get_props
        return get_props(x, y, z, t, c0)
    elif mms == 'mms4':
        from mms4 import get_props
        return get_props(x, y, z, t, c0)
    elif mms == 'mms5':
        from mms5 import get_props
        return get_props(x, y, z, t, c0)
    elif mms == 'mms6':
        from mms6 import get_props
        return get_props(x, y, z, t, c0)
    elif mms == 'mms7':
        from mms7 import get_props
        return get_props(x, y, z, t, c0)
    elif mms == 'mms8':
        from mms8 import get_props
        return get_props(x, y, z, t, c0)
    elif mms == 'mms9':
        from mms9 import get_props
        return get_props(x, y, z, t, c0)
    elif mms == 'mms10':
        from mms10 import get_props
        return get_props(x, y, z, t, c0)
    elif mms == 'mms11':
        from mms11 import get_props
        return get_props(x, y, z, t, c0)
    elif mms == 'mms12':
        from mms12 import get_props
        return get_props(x, y, z, t, c0)
    elif mms == 'mms13':
        from mms13 import get_props
        return get_props(x, y, z, t, c0)


def config_eq(eqns, mms='mms1'):
    if mms == 'mms1':
        from mms1 import config_eq
        return config_eq(eqns)
    elif mms == 'mms2':
        from mms2 import config_eq
        return config_eq(eqns)
    elif mms == 'mms3':
        from mms3 import config_eq
        return config_eq(eqns)
    elif mms == 'mms4':
        from mms4 import config_eq
        return config_eq(eqns)
    elif mms == 'mms5':
        from mms5 import config_eq
        return config_eq(eqns)
    elif mms == 'mms6':
        from mms6 import config_eq
        return config_eq(eqns)
    elif mms == 'mms7':
        from mms7 import config_eq
        return config_eq(eqns)
    elif mms == 'mms8':
        from mms8 import config_eq
        return config_eq(eqns)
    elif mms == 'mms9':
        from mms9 import config_eq
        return config_eq(eqns)
    elif mms == 'mms10':
        from mms10 import config_eq
        return config_eq(eqns)
    elif mms == 'mms11':
        from mms11 import config_eq
        return config_eq(eqns)
    elif mms == 'mms12':
        from mms12 import config_eq
        return config_eq(eqns)
    elif mms == 'mms13':
        from mms13 import config_eq
        return config_eq(eqns)
    else:
        return eqns


def create_particles_var_size(app, rho0, L):
    from pysph.base.utils import get_particle_array
    import os

    dim = app.dim
    dLi = 0.0
    dLf = L
    quarter = L/4

    dx = app.dx
    n_part = int((L/dx)**dim)

    while n_part > 1000000:
        print(quarter)
        dLi += quarter
        dLf -= quarter
        n_part = int(((dLf - dLi)/dx)**dim)
        quarter = quarter/2

    print('The domain is from %.6f to %.6f'%(dLi, dLf))
    print('The domain is of length %.6f'%(dLf-dLi))
    print('number of particles are %.1f'%((dLf-dLi)/dx))

    nl = 10 * dx
    xf, yf, zf = 0.0, 0.0, 0.0
    xs, ys, zs = 0.0, 0.0, 0.0
    cond = 0.0
    if dim == 2:
        xf, yf = np.mgrid[dLi+dx/2:dLf:dx, dLi+dx/2:dLf:dx]
        xf, yf = [np.ravel(t) for t in (xf, yf)]
        xs, ys = np.mgrid[dLi+dx/2-nl:dLf+nl:dx, dLi+dx/2-nl:dLf+nl:dx]
        cond = ~((xs > dLi) & (xs < dLf) & (ys > dLi) & (ys < dLf))
        zf = np.zeros_like(xf)
        zs = np.zeros_like(xs)
    elif dim == 3:
        xf, yf, zf = np.mgrid[dLi+dx/2:dLf:dx, dLi+dx/2:dLf:dx, dLi+dx/2:dLf:dx]
        xf, yf, zf = [np.ravel(t) for t in (xf, yf, zf)]
        xs, ys, zs = np.mgrid[dLi+dx/2-nl:dLf+nl:dx, dLi+dx/2-nl:dLf+nl:dx, dLi+dx/2-nl:dLf+nl:dx]
        xs, ys, zs = [np.ravel(t) for t in (xs, ys, zs)]
        cond = ~((xs > dLi) & (xs < dLf) & (ys > dLi) & (ys < dLf) & (zs > dLi) & (zs < dLf))


    # Initialize
    m = app.volume * rho0
    V0 = app.volume
    h = app.hdx * dx
    rho = rho0

    uf, vf, wf, rhocf, pf = get_props(xf, yf, zf, 0.0, app.c0, app.mms)
    if app.options.method == 'org':
        rho = rhocf.copy()
    fluid = get_particle_array(name='fluid', x=xf, y=yf, z=zf, m=m, h=h, rho=rho, rhoc=rhocf, u=uf, v=vf, w=wf, p=pf)

    particles = [fluid]

    cond0 = (ys > dLf)
    cond1 = cond & (~cond0)
    us0, vs0, ws0, rhocs0, ps0 = get_props(xs[cond0], ys[cond0], zs[cond0], 0.0, app.c0, app.mms)
    us1, vs1, ws1, rhocs1, ps1 = get_props(xs[cond1], ys[cond1], zs[cond1], 0.0, app.c0, app.mms)
    solid0 = get_particle_array(name='solid0', x=xs[cond0], y=ys[cond0], z=zs[cond0], m=m, h=h, rho=rho0, rhoc=rhocs0, u=us0, v=vs0, w=ws0, p=ps0)
    solid1 = get_particle_array(name='solid1', x=xs[cond1], y=ys[cond1], z=zs[cond1], m=m, h=h, rho=rho0, rhoc=rhocs1, u=us1, v=vs1, w=ws1, p=ps1)

    solid0.add_property('normal', stride=3)
    solid0.normal[0::3] = 0.0
    solid0.normal[1::3] = -1.0
    solid0.normal[2::3] = 0.0
    particles.append(solid0)
    particles.append(solid1)

    return particles


def create_particles(app, rho0, L):
    from pysph.base.utils import get_particle_array
    import os

    dx = app.dx
    nl = 10 * dx
    xf, yf = np.mgrid[dx/2:L:dx, dx/2:L:dx]
    xf, yf = [np.ravel(t) for t in (xf, yf)]
    if app.perturb == 'p':
        np.random.seed(10)
        xf += (np.random.random(len(xf)) - 0.5)/0.5 * app.dx * 0.1
        yf += (np.random.random(len(xf)) - 0.5)/0.5 * app.dx * 0.1
    elif app.perturb == 'pack':
        filename = os.path.join('code', 'mesh', 'nx%d.npz'%app.options.nx)
        data = np.load(filename)
        xf = data['x'] + 0.5
        yf = data['y'] + 0.5

    xs, ys = np.mgrid[dx/2-nl:L+nl:dx, dx/2-nl:L+nl:dx]
    cond = ~((xs > 0) & (xs < L) & (ys > 0) & (ys < L))
    print(len(cond))

    # Initialize
    m = app.volume * rho0
    V0 = app.volume
    h = app.hdx * dx
    rho = rho0

    uf, vf, wf, rhocf, pf = get_props(xf, yf, 0.0, 0.0, app.c0, app.mms)
    if app.options.method == 'org':
        rho = rhocf.copy()
    fluid = get_particle_array(name='fluid', x=xf, y=yf, m=m, h=h, rho=rho, rhoc=rhocf, u=uf, v=vf, w=wf, p=pf)

    particles = [fluid]

    cond0 = (ys > L)
    cond1 = cond & (~cond0)
    us0, vs0, ws0, rhocs0, ps0 = get_props(xs[cond0], ys[cond0], 0.0, 0.0, app.c0, app.mms)
    us1, vs1, ws1, rhocs1, ps1 = get_props(xs[cond1], ys[cond1], 0.0, 0.0, app.c0, app.mms)
    solid0 = get_particle_array(name='solid0', x=xs[cond0], y=ys[cond0], m=m, h=h, rho=rho0, rhoc=rhocs0, u=us0, v=vs0, w=ws0, p=ps0)
    solid1 = get_particle_array(name='solid1', x=xs[cond1], y=ys[cond1], m=m, h=h, rho=rho0, rhoc=rhocs1, u=us1, v=vs1, w=ws1, p=ps1)

    solid0.add_property('normal', stride=3)
    solid0.normal[0::3] = 0.0
    solid0.normal[1::3] = -1.0
    solid0.normal[2::3] = 0.0
    particles.append(solid0)
    particles.append(solid1)

    return particles

def create_particles_circular(app, rho0, L):
    from pysph.base.utils import get_particle_array
    from pysph.tools.geometry import remove_overlap_particles
    import os

    dx = app.dx
    nl = 10 * dx
    xf, yf = np.mgrid[dx/2-nl:L+nl:dx, dx/2-nl:L+nl:dx]
    xf, yf = [np.ravel(t) for t in (xf, yf)]
    theta = np.arctan2(yf-0.5, xf-0.5)

    r = 0.1*(3 + np.cos(4 * theta) + np.sin(2 * theta))
    cond = (xf-0.5)**2 + (yf-0.5)**2 - r**2 < 1e-14
    fx, fy = xf[cond], yf[cond]

    r = (0.1)*(3 + np.cos( 4 * theta) + np.sin(2 * theta)) +nl
    cond = (xf-0.5)**2 + (yf-0.5)**2 - r**2 < 1e-14
    sx, sy = xf[cond], yf[cond]

    # Initialize
    m = app.volume * rho0
    V0 = app.volume
    h = app.hdx * dx
    rho = rho0

    uf, vf, wf, rhocf, pf = get_props(fx, fy, 0.0, 0.0, app.c0, app.mms)
    if app.options.method == 'org':
        rho = rhocf.copy()
    fluid = get_particle_array(name='fluid', x=fx, y=fy, m=m, h=h, rho=rho, rhoc=rhocf, u=uf, v=vf, w=wf, p=pf)

    particles = [fluid]

    us0, vs0, ws0, rhocs0, ps0 = get_props(sx, sy, 0.0, 0.0, app.c0, app.mms)
    solid = get_particle_array(name='solid', x=sx, y=sy, m=m, h=h, rho=rho0, rhoc=rhocs0, u=us0, v=vs0, w=ws0, p=ps0)
    particles.append(solid)

    remove_overlap_particles(solid, fluid, dx, dim=app.dim)
    return particles

def create_particles_c_shape(app, rho0, L):
    from pysph.base.utils import get_particle_array
    from pysph.tools.geometry import remove_overlap_particles
    import os

    dx = app.dx
    nl = 10 * dx
    xf, yf = np.mgrid[dx/2-nl:L+nl:dx, dx/2-nl:L+nl:dx]
    xf, yf = [np.ravel(t) for t in (xf, yf)]

    r = 0.5
    v0 = [0.5, 0.75]
    origin = [0.5, 0.75 - r]
    u_v0 = [(v0[0] - origin[0]), (v0[1] - origin[1])]
    dist = np.sqrt((u_v0[0])**2 + (u_v0[1])**2)

    v1x = xf - origin[0]
    v1y = yf - origin[1]
    dist1 = np.sqrt((v1x)**2 + (v1y)**2)
    dotprod = (u_v0[0] * v1x + u_v0[1] * v1y)/dist/dist1
    cond = (yf > origin[1]) & (dotprod - 1/np.sqrt(2) > 1e-14) & (dist1 < r)
    fx, fy = xf[cond], yf[cond]

    r = 0.5 + 2 * nl
    v0 = [0.5, 0.75+ nl]
    origin = [0.5, 0.25 - nl]
    u_v0 = [(v0[0] - origin[0]), (v0[1] - origin[1])]
    dist = np.sqrt((u_v0[0])**2 + (u_v0[1])**2)

    v1x = xf - origin[0]
    v1y = yf - origin[1]
    dist1 = np.sqrt((v1x)**2 + (v1y)**2)
    dotprod = (u_v0[0] * v1x + u_v0[1] * v1y)/dist/dist1
    cond = (yf > origin[1]) & (dotprod - 1/np.sqrt(2) > 1e-14) & (dist1 < r)
    sx, sy = xf[cond], yf[cond]

    # Initialize
    m = app.volume * rho0
    V0 = app.volume
    h = app.hdx * dx
    rho = rho0

    uf, vf, wf, rhocf, pf = get_props(fx, fy, 0.0, 0.0, app.c0, app.mms)
    if app.options.method == 'org':
        rho = rhocf.copy()
    fluid = get_particle_array(name='fluid', x=fx, y=fy, m=m, h=h, rho=rho, rhoc=rhocf, u=uf, v=vf, w=wf, p=pf)

    particles = [fluid]

    us0, vs0, ws0, rhocs0, ps0 = get_props(sx, sy, 0.0, 0.0, app.c0, app.mms)
    solid = get_particle_array(name='solid', x=sx, y=sy, m=m, h=h, rho=rho0, rhoc=rhocs0, u=us0, v=vs0, w=ws0, p=ps0)
    particles.append(solid)

    remove_overlap_particles(solid, fluid, dx, dim=app.dim)
    return particles

def create_scheme(app, rho0, p0, c0):
    from pysph.sph.scheme import SchemeChooser
    from tsph_with_pst import TSPHScheme
    from tsph_dsph import TSPHWithDSPHScheme
    from ewcsph import EWCSPHScheme

    from pysph.sph.wc.edac import EDACScheme
    tsph = TSPHScheme(
        ['fluid'], ['solid0', 'solid1'], dim=2, rho0=rho0,
        hdx=None, nu=None, gamma=7.0, kernel_corr=True
    )
    tsph_pie = TSPHScheme(
        ['fluid'], ['solid'], dim=2, rho0=rho0,
        hdx=None, nu=None, gamma=7.0, kernel_corr=True
    )
    tsph_p = TSPHScheme(
        ['fluid'], ['solid0', 'solid1'], dim=2, rho0=rho0,
        hdx=None, nu=None, gamma=7.0, kernel_corr=True, bcs={'p_solid':['solid0'], 'mms':['solid1']}
    )
    tsph_uns = TSPHScheme(
        ['fluid'], ['solid0', 'solid1'], dim=2, rho0=rho0,
        hdx=None, nu=None, gamma=7.0, kernel_corr=True, bcs={'u_no_slip':['solid0'], 'mms':['solid1']}
    )
    tsph_us = TSPHScheme(
        ['fluid'], ['solid0', 'solid1'], dim=2, rho0=rho0,
        hdx=None, nu=None, gamma=7.0, kernel_corr=True, bcs={'u_slip':['solid0'], 'mms':['solid1']}
    )
    tsph_dirc_u = TSPHScheme(
        ['fluid'], ['solid0', 'solid1'], dim=2, rho0=rho0,
        hdx=None, nu=None, gamma=7.0, kernel_corr=True, bcs={'u_dir':['solid0'], 'mms':['solid1']}
    )
    tsph_dirc_p = TSPHScheme(
        ['fluid'], ['solid0', 'solid1'], dim=2, rho0=rho0,
        hdx=None, nu=None, gamma=7.0, kernel_corr=True, bcs={'p_dir':['solid0'], 'mms':['solid1']}
    )
    ewcsph = EWCSPHScheme(
        ['fluid'], ['solid0', 'solid1'], dim=2, rho0=rho0, c0=c0, h0=None,
        hdx=None, nu=None, gamma=7.0, kernel_corr=True
    )
    tdsph = TSPHWithDSPHScheme(
        ['fluid'], ['solid0', 'solid1'], dim=2, rho0=rho0, c0=c0, h0=None,
        hdx=None, nu=None, gamma=7.0, kernel_corr=True
    )
    edac = EDACScheme(
        ['fluid'], ['solid0', 'solid1'], dim=2, rho0=rho0,
        h=None, nu=None, c0=c0)

    s = SchemeChooser(
        default='tsph', tsph=tsph, edac=edac, tdsph=tdsph, tsph_p=tsph_p,
        tsph_us=tsph_us, tsph_uns=tsph_uns, tsph_dirc_u=tsph_dirc_u,
        tsph_dirc_p=tsph_dirc_p, tsph_pie=tsph_pie, ewcsph=ewcsph
    )

    return s

def configure_scheme(app, rho0, p0, c0):
    from pysph.base.kernels import QuinticSpline
    kernel = QuinticSpline(dim=app.dim)
    h = app.dx * app.hdx

    scheme_split = app.options.scheme.split('_')

    if scheme_split[0] == 'tsph':
        app.scheme.configure(hdx=app.hdx, nu=app.nu, dim=app.dim)
    elif app.options.scheme == 'ewcsph':
        app.scheme.configure(hdx=app.hdx, nu=app.nu, dim=app.dim)
    elif app.options.scheme == 'edac':
        app.scheme.configure(h=h, nu=app.nu, dim=app.dim)
    elif app.options.scheme == 'tdsph':
        app.scheme.configure(hdx=app.hdx, nu=app.nu, h0=h, dim=app.dim)

    app.scheme.configure_solver(kernel=kernel, tf=app.tf, dt=app.dt)


def create_equations(app):
    eqns = app.scheme.get_equations()
    if app.options.scheme == 'edac':
        eqns = add_mms_equations_edac(eqns)
    eqns = config_eq(eqns, mms=app.mms)
    print(eqns)
    return eqns

def add_mms_equations_edac(eqns):
    from pysph.sph.wc.transport_velocity import SummationDensity
    eq1 = SetValuesonSolid(dest = 'solid0', sources=None)
    eq1_1 = SetValuesonSolid(dest = 'solid1', sources=None)
    eq2 = AddMomentumSourceTerm(dest='fluid', sources=None)
    eq3 = AddPressureEvolutionSourceTerm(dest='fluid', sources=None)
    eq4 = SummationDensity(dest='solid0', sources=['fluid', 'solid0', 'solid1'])
    eq4_1 = SummationDensity(dest='solid1', sources=['fluid', 'solid0', 'solid1'])
    eq5 = XSPHCorrectionDummy(dest='fluid', sources=None)

    eqns[0].equations.pop(-1)
    eqns[0].equations.pop(-1)
    eqns[0].equations.pop(-1)
    eqns[0].equations.pop(-1)
    eqns[1].equations.pop(-1)

    eqns[0].equations.append(eq4)
    eqns[0].equations.append(eq4_1)
    eqns.insert(1, Group(equations=[eq1, eq1_1]))
    eqns[2].equations.append(eq2)
    eqns[2].equations.append(eq3)
    eqns[2].equations.append(eq5)


    return eqns