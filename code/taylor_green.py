#Taylor Green problem

from pysph.examples.taylor_green import TaylorGreen, exact_solution
from pysph.sph.scheme import SchemeChooser
from pysph.base.kernels import QuinticSpline
from pysph.base.utils import get_particle_array

from tsph_with_pst import TSPHScheme

L = 1.0
U = 1.0
rho0 = 1.0
c0 = 10 * U
p0 = c0**2 * rho0

class TG(TaylorGreen):
    def create_scheme(self):
        tsph = TSPHScheme(
        ['fluid'], [], dim=2, rho0=1.0,
        hdx=None, nu=None, gamma=7.0, kernel_corr=True)

        s = SchemeChooser(
            default='tsph', tsph=tsph
        )
        return s

    def configure_scheme(self):
        scheme = self.scheme
        h0 = self.hdx * self.dx
        pfreq = 100
        kernel = QuinticSpline(dim=2)
        if self.options.scheme == 'tsph':
            scheme.configure(hdx=self.hdx, nu=self.nu)
        scheme.configure_solver(kernel=kernel, tf=self.tf, dt=self.dt,
                                pfreq=pfreq)

    def create_fluid(self):
        import numpy as np
        # create the particles
        dx = self.dx
        _x = np.arange(dx / 2, L, dx)
        x, y = np.meshgrid(_x, _x)
        if self.options.init is not None:
            fname = self.options.init
            from pysph.solver.utils import load
            data = load(fname)
            _f = data['arrays']['fluid']
            x, y = _f.x.copy(), _f.y.copy()

        if self.options.perturb > 0:
            np.random.seed(1)
            factor = dx * self.options.perturb
            x += np.random.random(x.shape) * factor
            y += np.random.random(x.shape) * factor

        # Initialize
        m = self.volume * rho0
        h = self.hdx * dx
        re = self.options.re
        b = -8.0*np.pi*np.pi / re
        u0, v0, p0 = exact_solution(U=U, b=b, t=0, x=x, y=y)
        color0 = np.cos(2*np.pi*x) * np.cos(4*np.pi*y)

        # create the arrays
        fluid = get_particle_array(name='fluid', x=x, y=y, m=m, h=h, u=u0,
                                   v=v0, rho=rho0, p=p0, color=color0)
        return fluid

    def create_particles(self):
        import numpy as np
        fluid = self.create_fluid()

        self.scheme.setup_properties([fluid])

        nfp = fluid.get_number_of_particles()
        fluid.gid[:] = np.arange(nfp)
        fluid.add_constant('c0', 10)
        fluid.add_property('gradv', stride=9)
        fluid.rhoc[:] = fluid.p/c0**2 + rho0
        return [fluid]

    def create_equations(self):
        eq = self.scheme.get_equations()
        print(eq)
        return eq

    def post_step(self, solver):
        from config_mms import config_eq
        self.scheme.scheme.post_step(self.particles, self.domain, config_eq, mms=' ')


if __name__ == "__main__":
    app = TG()
    app.run()
    app.post_process(app.info_filename)