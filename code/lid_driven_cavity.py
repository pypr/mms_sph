# lid driven cavity

from pysph.examples.cavity import LidDrivenCavity
from math import sqrt
from compyle.api import declare
from pysph.sph.equation import Equation, Group
from pysph.sph.scheme import SchemeChooser
from pysph.base.kernels import QuinticSpline
from pysph.base.utils import get_particle_array

from tsph_with_pst import TSPHScheme

# domain and reference values
L = 1.0
Umax = 1.0
c0 = 10 * Umax
rho0 = 1.0
p0 = c0 * c0 * rho0

# Numerical setup
hdx = 1.0


class ComputeNormals(Equation):

    """Compute normals using a simple approach

    .. math::

       -\frac{m_j}{\rho_j} DW_{ij}

    First compute the normals, then average them and finally normalize them.
    from pypr/sisph
    """

    def initialize(self, d_idx, d_normal_tmp, d_normal):
        idx = declare('int')
        idx = 3*d_idx
        d_normal_tmp[idx] = 0.0
        d_normal_tmp[idx + 1] = 0.0
        d_normal_tmp[idx + 2] = 0.0
        d_normal[idx] = 0.0
        d_normal[idx + 1] = 0.0
        d_normal[idx + 2] = 0.0

    def loop(self, d_idx, d_normal_tmp, s_idx, s_m, s_rho, DWIJ):
        idx = declare('int')
        idx = 3*d_idx
        fac = -s_m[s_idx]/s_rho[s_idx]
        d_normal_tmp[idx] += fac*DWIJ[0]
        d_normal_tmp[idx + 1] += fac*DWIJ[1]
        d_normal_tmp[idx + 2] += fac*DWIJ[2]

    def post_loop(self, d_idx, d_normal_tmp, d_h):
        idx = declare('int')
        idx = 3*d_idx
        mag = sqrt(d_normal_tmp[idx]**2 + d_normal_tmp[idx + 1]**2 +
                   d_normal_tmp[idx + 2]**2)
        if mag > 0.25/d_h[d_idx]:
            d_normal_tmp[idx] /= mag
            d_normal_tmp[idx + 1] /= mag
            d_normal_tmp[idx + 2] /= mag
        else:
            d_normal_tmp[idx] = 0.0
            d_normal_tmp[idx + 1] = 0.0
            d_normal_tmp[idx + 2] = 0.0


class SmoothNormals(Equation):
    def loop(self, d_idx, d_normal, s_normal_tmp, s_idx, s_m, s_rho, WIJ):
        idx = declare('int')
        idx = 3*d_idx
        fac = s_m[s_idx]/s_rho[s_idx]*WIJ
        d_normal[idx] += fac*s_normal_tmp[3*s_idx]
        d_normal[idx + 1] += fac*s_normal_tmp[3*s_idx + 1]
        d_normal[idx + 2] += fac*s_normal_tmp[3*s_idx + 2]

    def post_loop(self, d_idx, d_normal, d_h):
        idx = declare('int')
        idx = 3*d_idx
        mag = sqrt(d_normal[idx]**2 + d_normal[idx + 1]**2 +
                   d_normal[idx + 2]**2)
        if mag > 1e-3:
            d_normal[idx] /= mag
            d_normal[idx + 1] /= mag
            d_normal[idx + 2] /= mag
        else:
            d_normal[idx] = 0.0
            d_normal[idx + 1] = 0.0
            d_normal[idx + 2] = 0.0


class LDC(LidDrivenCavity):

    def _get_normals(self, pa):
        from pysph.tools.sph_evaluator import SPHEvaluator
        import numpy

        pa.add_property('normal', stride=3)
        pa.add_property('normal_tmp', stride=3)

        name = pa.name

        props = ['m', 'rho', 'h']
        for p in props:
            x = pa.get(p)
            if numpy.all(x < 1e-12):
                msg = f'WARNING: cannot compute normals "{p}" is zero'
                print(msg)

        seval = SPHEvaluator(
            arrays=[pa], equations=[
                Group(equations=[
                    ComputeNormals(dest=name, sources=[name])
                ]),
                Group(equations=[
                    SmoothNormals(dest=name, sources=[name])
                ]),
            ],
            dim=2, domain_manager=self.domain
        )
        seval.evaluate()

    def create_scheme(self):
        tsph = TSPHScheme(
        ['fluid'], ['solid'], dim=2, rho0=1.0,
        hdx=None, nu=None, gamma=7.0, kernel_corr=True,
        bcs={'all':['solid']})

        s = SchemeChooser(
            default='tsph', tsph=tsph
        )
        return s

    def configure_scheme(self):
        scheme = self.scheme
        h0 = hdx * self.dx
        pfreq = 100
        kernel = QuinticSpline(dim=2)
        if self.options.scheme == 'tsph':
            scheme.configure(hdx=hdx, nu=self.nu)
        scheme.configure_solver(kernel=kernel, tf=self.tf, dt=self.dt,
                                pfreq=pfreq)

    def create_particles(self):
        import numpy as np
        dx = self.dx
        ghost_extent = 5 * dx
        # create all the particles
        _x = np.arange(-ghost_extent - dx / 2, L + ghost_extent + dx / 2, dx)
        x, y = np.meshgrid(_x, _x)
        x = x.ravel()
        y = y.ravel()

        # sort out the fluid and the solid
        indices = []
        for i in range(x.size):
            if ((x[i] > 0.0) and (x[i] < L)):
                if ((y[i] > 0.0) and (y[i] < L)):
                    indices.append(i)

        # create the arrays
        solid = get_particle_array(name='solid', x=x, y=y, m=rho0*dx**2, h=hdx*dx, rho=rho0)

        # remove the fluid particles from the solid
        fluid = solid.extract_particles(indices)
        fluid.set_name('fluid')
        solid.remove_particles(indices)


        print("Lid driven cavity :: Re = %d, dt = %g" % (self.re, self.dt))

        y = solid.y
        cond = y > 1.0
        solid.u[cond] = 1.0
        self._get_normals(solid)

        particles = [solid, fluid]

        self.scheme.setup_properties([fluid, solid])

        for i, pa in enumerate(particles):
            pa.add_constant('c0', 10)
            pa.add_property('gradv', stride=9)
            pa.rhoc[:] = pa.p/c0**2 + rho0

        return particles

    def create_equations(self):
        eq = self.scheme.get_equations()
        print(eq)
        return eq

    def post_step(self, solver):
        from config_mms import config_eq
        self.scheme.scheme.post_step(self.particles, self.domain, config_eq, mms=' ')


if __name__ == "__main__":
    app = LDC()
    app.run()
    app.post_process(app.info_filename)