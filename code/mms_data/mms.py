from numpy.lib.npyio import save
from code.mms_data.base import (
    momentum_eq, continuity, save_latex, save_formula, vec_grad, pressure_evol)
import sympy as sp
import os
import yaml

x, y, z, us, vs, ps, rhocs, t = sp.symbols('x, y, z, u, v, p, rhoc, t')
rhoc0, c0 = sp.symbols('rhoc0, c0')
rho0, rho = sp.symbols('rho0, rho')
PI = sp.pi
COS = sp.cos
SIN = sp.sin
EXP = sp.exp

yaml_dict = {"tex":{}, "py":{}}

def mms1():
    # testing the inviscid solver is second order accurate
    print('creating mms1')
    u = SIN(2*PI*x) * COS(2*PI*y)
    v = -COS(2*PI*x) * SIN(2*PI*y)
    p = COS(4*PI*x) + COS(4*PI*y)
    rhoc = p/c0**2 + rhoc0

    su = momentum_eq([u, v, 0], rhoc, p, rhocs, nu=0.0, comp=0)
    sv = momentum_eq([u, v, 0], rhoc, p, rhocs, nu=0.0, comp=1)
    srho = continuity(rhoc, [u, v, 0], rhocs)
    spp = pressure_evol(p, [u, v, 0], rhocs)
    gradv = vec_grad([u, v, 0]) 

    save_latex(u, v, p, rhoc, su, sv, srho, spp, 'mms1', yaml_dict)
    save_formula(u, v, p, rhoc, su, sv, srho, gradv, spp, 'mms1', yaml_dict)


def mms2():
    #testing the viscous term is second order accurate
    print('creating mms2')
    u = EXP(-10*t)*SIN(2*PI*x) * COS(2*PI*y)*y**2
    v = -EXP(-10*t)*COS(2*PI*x) * SIN(2*PI*y)
    p = EXP(-10*t)* (COS(4*PI*x) + COS(4*PI*y))
    rhoc = p/c0**2 + rhoc0

    su = momentum_eq([u, v, 0], rhoc, p, rhocs, nu=0.25, comp=0)
    sv = momentum_eq([u, v, 0], rhoc, p, rhocs, nu=0.25, comp=1)
    srho = continuity(rhoc, [u, v, 0], rhocs)
    spp = pressure_evol(p, [u, v, 0], rhocs)
    gradv = vec_grad([u, v, 0]) 

    save_latex(u, v, p, rhoc, su, sv, srho, spp, 'mms2', yaml_dict)
    save_formula(u, v, p, rhoc, su, sv, srho, gradv, spp, 'mms2', yaml_dict)


def mms3():
    #testing the viscous term is second order accurate
    print('creating mms3')
    u = EXP(-10*t)*SIN(2*PI*x) * COS(2*PI*y)
    v = -EXP(-10*t)*COS(2*PI*x) * SIN(2*PI*y)
    p = EXP(-10*t)*(COS(4*PI*x) + COS(4*PI*y))
    rhoc = p/c0**2 + rhoc0

    su = momentum_eq([u, v, 0], rhoc, p, rhocs, nu=0.01, comp=0)
    sv = momentum_eq([u, v, 0], rhoc, p, rhocs, nu=0.01, comp=1)
    srho = continuity(rhoc, [u, v, 0], rhocs)
    spp = pressure_evol(p, [u, v, 0], rhocs)
    gradv = vec_grad([u, v, 0]) 

    save_latex(u, v, p, rhoc, su, sv, srho, spp, 'mms3', yaml_dict)
    save_formula(u, v, p, rhoc, su, sv, srho, gradv, spp, 'mms3', yaml_dict)

def mms4():
    #testing the dirichlet boundary 
    print('creating mms4')
    u = SIN(2*PI*x) * COS(2*PI*y)*y**2
    v = -COS(2*PI*x) * SIN(2*PI*y)
    p = (COS(4*PI*x) + COS(4*PI*y)) * (y-1)**2
    rhoc = p/c0**2 + rhoc0

    su = momentum_eq([u, v, 0], rhoc, p, rhocs, nu=0.0, comp=0)
    sv = momentum_eq([u, v, 0], rhoc, p, rhocs, nu=0.0, comp=1)
    srho = continuity(rhoc, [u, v, 0], rhocs)
    spp = pressure_evol(p, [u, v, 0], rhocs)
    gradv = vec_grad([u, v, 0]) 

    save_latex(u, v, p, rhoc, su, sv, srho, spp, 'mms4', yaml_dict)
    save_formula(u, v, p, rhoc, su, sv, srho, gradv, spp, 'mms4', yaml_dict)


def mms5():
    # testing the rho compared with rhoc for EDAC 
    print('creating mms5')
    u = SIN(2*PI*x) * COS(2*PI*y)
    v = -COS(2*PI*x) * SIN(2*PI*y)
    p = COS(4*PI*x) + COS(4*PI*y)
    rhoc = p/c0**2 + rho0

    su = momentum_eq([u, v, 0], rhoc, p, rho, nu=0.0, comp=0)
    sv = momentum_eq([u, v, 0], rhoc, p, rho, nu=0.0, comp=1)
    srho = continuity(rhoc, [u, v, 0], rho)
    spp = pressure_evol(p, [u, v, 0], rho, delta_coeff=0.5)
    gradv = vec_grad([u, v, 0])

    save_latex(u, v, p, rhoc, su, sv, srho, spp, 'mms5', yaml_dict)
    save_formula(u, v, p, rhoc, su, sv, srho, gradv, spp, 'mms5', yaml_dict)


def mms6():
    # testing testing the viscid boundary condition 
    print('creating mms6')
    u = EXP(-10*t) * SIN(2*PI*x) * COS(2*PI*y) * (y-1)**2
    v = -EXP(-10*t) * COS(2*PI*x) * SIN(2*PI*y) * (y-1)**2
    p = EXP(-10*t) * (COS(4*PI*x) + COS(4*PI*y))
    rhoc = p/c0**2 + rho0

    su = momentum_eq([u, v, 0], rhoc, p, rhocs, nu=1.0, comp=0)
    sv = momentum_eq([u, v, 0], rhoc, p, rhocs, nu=1.0, comp=1)
    srho = continuity(rhoc, [u, v, 0], rhocs)
    spp = pressure_evol(p, [u, v, 0], rhocs)
    gradv = vec_grad([u, v, 0]) 

    save_latex(u, v, p, rhoc, su, sv, srho, spp, 'mms6', yaml_dict)
    save_formula(u, v, p, rhoc, su, sv, srho, gradv, spp, 'mms6', yaml_dict)


def mms7():
    # testing testing the slip boundary condition 
    print('creating mms7')
    u = SIN(2*PI*x) * COS(2*PI*y)*(y-1)**2
    v = -COS(2*PI*x) * SIN(2*PI*y)
    p = (COS(4*PI*x) + COS(4*PI*y)) * (y-1)
    rhoc = p/c0**2 + rho0

    su = momentum_eq([u, v, 0], rhoc, p, rhocs, nu=0.0, comp=0)
    sv = momentum_eq([u, v, 0], rhoc, p, rhocs, nu=0.0, comp=1)
    srho = continuity(rhoc, [u, v, 0], rhocs)
    spp = pressure_evol(p, [u, v, 0], rhocs)
    gradv = vec_grad([u, v, 0]) 

    save_latex(u, v, p, rhoc, su, sv, srho, spp, 'mms7', yaml_dict)
    save_formula(u, v, p, rhoc, su, sv, srho, gradv, spp, 'mms7', yaml_dict)


def mms8():
    #testing dirichlet boundary condition 
    print('creating mms8')
    u = SIN(2*PI*x) * COS(2*PI*y) * (y-1)
    v = -COS(2*PI*x) * SIN(2*PI*y) * (y-1)
    p = (COS(4*PI*x) + COS(4*PI*y)) * (y-1)
    rhoc = p/c0**2 + rhoc0

    su = momentum_eq([u, v, 0], rhoc, p, rhocs, nu=0.0, comp=0)
    sv = momentum_eq([u, v, 0], rhoc, p, rhocs, nu=0.0, comp=1)
    srho = continuity(rhoc, [u, v, 0], rhocs)
    spp = pressure_evol(p, [u, v, 0], rhocs)
    gradv = vec_grad([u, v, 0]) 

    save_latex(u, v, p, rhoc, su, sv, srho, spp, 'mms8', yaml_dict)
    save_formula(u, v, p, rhoc, su, sv, srho, gradv, spp, 'mms8', yaml_dict)

def mms9():
    #testing dirichlet boundary condition 
    print('creating mms9')
    u = 0.0 
    v = 0.0 
    p = 0.0 
    rhoc = rhoc0

    for i in range(1,10):
        u += EXP(-10*t) * SIN(2*i*PI*x) * COS(2*i*PI*y)*y**2
        v += -EXP(-10*t) * COS(2*i*PI*x) * SIN(2*i*PI*y)
        p += EXP(-10*t) * (COS(4*i*PI*x) + COS(4*i*PI*y))

    rhoc += p/c0**2

    su = momentum_eq([u, v, 0], rhoc, p, rhocs, nu=.01, comp=0)
    sv = momentum_eq([u, v, 0], rhoc, p, rhocs, nu=.01, comp=1)
    srho = continuity(rhoc, [u, v, 0], rhocs)
    spp = pressure_evol(p, [u, v, 0], rhocs)
    gradv = vec_grad([u, v, 0]) 

    save_latex(u, v, p, rhoc, su, sv, srho, spp, 'mms9', yaml_dict)
    save_formula(u, v, p, rhoc, su, sv, srho, gradv, spp, 'mms9', yaml_dict)

def mms10():
    #testing dirichlet boundary condition 
    print('creating mms10')
    u = 0.0 
    v = 0.0 
    p = 0.0 
    rhoc = rhoc0

    for i in range(1,10):
        u += EXP(-10*t) * SIN(2*i*PI*x) * COS(2*i*PI*y)*y**2
        v += -EXP(-10*t) * COS(2*i*PI*x) * SIN(2*i*PI*y)
        p += EXP(-10*t) * (COS(4*i*PI*x) + COS(4*i*PI*y))

    rhoc += p/c0**2

    su = momentum_eq([u, v, 0], rhoc, p, rhocs, nu=0.0, comp=0)
    sv = momentum_eq([u, v, 0], rhoc, p, rhocs, nu=0.0, comp=1)
    srho = continuity(rhoc, [u, v, 0], rhocs)
    spp = pressure_evol(p, [u, v, 0], rhocs)
    gradv = vec_grad([u, v, 0]) 

    save_latex(u, v, p, rhoc, su, sv, srho, spp, 'mms10', yaml_dict)
    save_formula(u, v, p, rhoc, su, sv, srho, gradv, spp, 'mms10', yaml_dict)



def mms11():
    #testing the viscous term with divergence in velocity 
    print('creating mms11')
    u = EXP(-10*t)*SIN(2*PI*x) * COS(2*PI*y)*y**2
    v = -EXP(-10*t)*COS(2*PI*x) * SIN(2*PI*y)
    p = EXP(-10*t)*(COS(4*PI*x) + COS(4*PI*y))
    rhoc = p/c0**2 + rhoc0

    su = momentum_eq([u, v, 0], rhoc, p, rhocs, nu=0.01, comp=0)
    sv = momentum_eq([u, v, 0], rhoc, p, rhocs, nu=0.01, comp=1)
    srho = continuity(rhoc, [u, v, 0], rhocs)
    spp = pressure_evol(p, [u, v, 0], rhocs)
    gradv = vec_grad([u, v, 0]) 

    save_latex(u, v, p, rhoc, su, sv, srho, spp, 'mms11', yaml_dict)
    save_formula(u, v, p, rhoc, su, sv, srho, gradv, spp, 'mms11', yaml_dict)


def mms12():
    #testing the 3D domain 
    print('creating mms12')
    u = EXP(-10*t)*SIN(2*PI*(x+z)) * COS(2*PI*(y+x))*y**2
    v = -EXP(-10*t)*COS(2*PI*(x+y)) * SIN(2*PI*(y+z))
    w = -EXP(-10*t)*COS(2*PI*(y+z)) * SIN(2*PI*(z+x))
    p = EXP(-10*t)*(COS(4*PI*(x+z)) + COS(4*PI*(y+x)))
    rhoc = p/c0**2 + rhoc0

    su = momentum_eq([u, v, w], rhoc, p, rhocs, nu=0.01, comp=0)
    sv = momentum_eq([u, v, w], rhoc, p, rhocs, nu=0.01, comp=1)
    sw = momentum_eq([u, v, w], rhoc, p, rhocs, nu=0.01, comp=2)
    srho = continuity(rhoc, [u, v, w], rhocs)
    spp = pressure_evol(p, [u, v, w], rhocs)
    gradv = vec_grad([u, v, w]) 

    save_latex(u, v, p, rhoc, su, sv, srho, spp, 'mms12', yaml_dict, sw=sw, w=w)
    save_formula(u, v, p, rhoc, su, sv, srho, gradv, spp, 'mms12', yaml_dict, sw=sw, w=w)

def mms13():
    # testing the L-IPST-C edac scheme
    print('creating mms13')
    u = SIN(2*PI*x) * COS(2*PI*y)
    v = -COS(2*PI*x) * SIN(2*PI*y)
    p = COS(4*PI*x) + COS(4*PI*y)
    rhoc = p/c0**2 + rhoc0

    su = momentum_eq([u, v, 0], rhoc, p, rhocs, nu=0.0, comp=0)
    sv = momentum_eq([u, v, 0], rhoc, p, rhocs, nu=0.0, comp=1)
    srho = continuity(rhoc, [u, v, 0], rhocs)
    spp = pressure_evol(p, [u, v, 0], rhocs, delta_coeff=0.5)
    gradv = vec_grad([u, v, 0]) 

    save_latex(u, v, p, rhoc, su, sv, srho, spp, 'mms13', yaml_dict)
    save_formula(u, v, p, rhoc, su, sv, srho, gradv, spp, 'mms13', yaml_dict)


mms1()
mms2()
mms3()
mms4()
mms5()
mms6()
mms7()
mms8()
mms9()
mms10()
mms11()
mms12()
mms13()
folder = os.path.dirname(os.path.abspath(__file__))
filename = os.path.join(folder, 'mms.yaml')
fp = open(filename, 'w')
yaml.dump(yaml_dict, fp)
fp.close()