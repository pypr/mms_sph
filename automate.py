#!/usr/bin/env python
import os

from automan.api import PySPHProblem
from automan.api import Automator, Simulation, filter_cases
from automan.automation import Problem
import numpy as np
import matplotlib
matplotlib.use('pdf')
from matplotlib import pyplot as plt


params = {'legend.fontsize': 'X-large',
         'axes.labelsize': 'X-large',
         'axes.titlesize':'X-large',
         'xtick.labelsize':'X-large',
         'ytick.labelsize':'X-large'}
plt.rcParams.update(params)


BASE_HDX = 1.2
HDX = 1.2
RESOLUTIONS = [50, 100, 200, 250, 400, 500, 1000]
RESOLUTIONS2 = [100, 200, 250, 400, 500, 1000, 2000, 4000, 8000]
# RESOLUTIONS = [50, 100, 200]
# RESOLUTIONS = [100, 200]
pfreq = 20
CLEAN = True
NCORE, NTHREAD = 12, 80


def get_convergence_data(cases, nx='nx', error='l1'):
    data = {}
    for case in cases:
        l1 = case.data[error]
        l1 = sum(l1)/len(l1)
        data[case.params[nx]] = l1
    nx_arr = np.asarray(sorted(data.keys()), dtype=float)
    l1 = np.asarray([data[x] for x in nx_arr])
    return nx_arr, l1


def get_time_taken(cases, nx='nx'):
    from automate import get_cpu_time
    data = {}
    for case in cases:
        time = get_cpu_time(case)
        data[case.params[nx]] = time
    nx_arr = np.asarray(sorted(data.keys()), dtype=float)
    times = np.asarray([data[x] for x in nx_arr])
    return nx_arr, times


def make_table(column_names, row_data, output_fname, sort_cols=None,
               multicol=None, **extra_kw):
    import pandas as pd
    col_data = [[] for i in range(len(column_names))]
    for row in row_data:
        for i, item in enumerate(row):
            col_data[i].append(item)
    d = {n: col_data[i] for i, n in enumerate(column_names)}
    df = pd.DataFrame(d, columns=column_names)
    if sort_cols:
        sort_by = [column_names[i] for i in sort_cols]
    else:
        sort_by = column_names[-1]
    df.sort_values(by=sort_by, inplace=True)
    parent = os.path.dirname(output_fname)
    if not os.path.exists(parent):
        os.mkdir(parent)
    with open(output_fname, 'w') as fp:
        fp.write(df.to_latex(index=False, escape=False, **extra_kw))
        fp.close()
    if multicol is not None:
        import fileinput
        for line in fileinput.FileInput(output_fname, inplace=1):
            if "\\toprule" in line:
                line = line.replace(line, multicol)
            print(line, end='')

class MMSConv(PySPHProblem):
    def __init__(
        self, sim, out, tf=2.0, common_cmd='', name='', cores=0, threads=0,
        err=['l1', 'p_l1'], res=RESOLUTIONS):
        self.tf = tf
        self.cmd = common_cmd
        self.name = name
        self.cores = cores
        self.threads = threads
        self.errs = err
        self.res = res

        super(MMSConv, self).__init__(sim, out)

    def _make_cases(self, cmd, tf_override=None, hdx_override=0.0):
        get_path = self.input_path
        self.nx = self.res
        self.re = [100]
        self.kernel = 'QuinticSpline'
        self.hdx = HDX
        if hdx_override > 0.1:
            self.hdx = hdx_override

        tf = self.tf
        if tf_override is not None:
            tf = tf_override

        n_core = self.cores
        n_thread = self.threads

        cmd_split = cmd.split()
        cmd_extra = self.cmd.split()

        # remove commands already in the base class
        ##########
        to_remove = []
        for i in range(int(len(cmd_extra)/2)):
            if cmd_extra[2*i] in cmd_split:
                to_remove.append(cmd_extra[2*i])
                to_remove.append(cmd_extra[2*i+1])
        for _cmd in to_remove:
            cmd_extra.remove(_cmd)

        extra = ' '
        for _cmd in cmd_extra:
            extra += _cmd
            extra += ' '
        ##########

        _cmd = 'python code/mms_main.py --openmp' + cmd + extra
        cases = [
            Simulation(
                get_path('re_%s_nx_%d' % (re, nx)), _cmd,
                job_info=dict(
                    n_core=n_core, n_thread=n_thread), nx=nx, re=re, tf=tf,
                    kernel=self.kernel, pfreq=pfreq, hdx=self.hdx
            )
            for re in self.re for nx in self.nx
        ]

        return cases

    def run(self):
        self.make_output_dir()
        self._plot_convergence_rate(error='l1')
        self._plot_convergence_rate(error='p_l1')
        self._plot_convergence_rate(error='linf')
        self._plot_convergence_rate(error='p_linf')

    def _plot_convergence_rate(self, error='l1'):
        import matplotlib.pyplot as plt
        style = ['g-o', 'b-o', 'r-o', 'k-0']
        plt.figure(1)
        plt.figure(2)
        for re, ls in zip(self.re, style):
            cases = filter_cases(self.cases, re=re)
            nx, l1 = get_convergence_data(cases, nx='nx', error=error)
            plt.figure(1)
            plt.loglog(1.0/nx, l1, ls, label='Re=%s' % re)
            hdx = self.hdx#get_new_hdx2(nx, BASE_HDX, 50)
            dx = 1.0/(nx)*hdx
            ldx = np.log(dx)
            ll1 = np.log(l1)
            slope = (ll1[1:] - ll1[:-1])/(ldx[1:] - ldx[:-1])
            plt.figure(2)
            plt.plot(dx[1:], slope, ls, label='Re=%s' % re)

        plt.figure(1)
        # plt.loglog(1.0/nx, (5./nx)**2, 'k--', linewidth=2,
        #            label=r'Expected $O(h^2)$')
        # plt.loglog(1.0/nx, (5./nx), 'k:', linewidth=2,
        #            label=r'Expected $O(h)$')
        # plt.loglog(1.0/nx, np.power(5./nx, 0.5), 'k-.', linewidth=2,
        #            label=r'Expected $O(h^{0.5})$')
        plt.legend(loc='best')
        plt.xlabel(r'$h$')
        plt.ylabel(r'$L_1$ error')
        # plt.xlim(0.005, 0.1)
        plt.savefig(self.output_path('%s_conv.pdf')%error)
        plt.close(1)

        plt.figure(2)
        plt.legend(loc='best')
        plt.xlabel(r'$h$')
        plt.ylabel(r'Convergence order')
        # plt.xlim(0.005, 0.1)
        plt.savefig(self.output_path('%s_conv_order.pdf')%error)
        plt.close(2)


class Comparison(PySPHProblem):
    def _make_case(self, klass, tf=2.0, common_cmd='', name='',
                   threads=NTHREAD, cores=NCORE, err=['p_l1', 'l1'],
                   res=RESOLUTIONS):
        self.err = err
        self.problems = {
            cls.__name__: cls(
                self.sim_dir, self.out_dir, tf=tf, common_cmd=common_cmd,
                name=name, cores=cores, threads=threads, err=err, res=res)
            for cls in klass
        }

    def _set_re(self, name):
        self.re = self.problems[name].re
        self.nx = self.problems[name].nx
        self.hdx = self.problems[name].hdx

    def get_label(self, problem):
        return ''

    def get_requires(self):
        return list(self.problems.items())

    def run(self):
        self.make_output_dir()
        self._plot_convergence_rate(err=self.err)
        self.remove_data()

    def remove_data(self):
        if CLEAN:
            import glob
            import os
            for problem in self.problems.values():
                cases = problem.cases
                for case in cases:
                    dirname = case.input_path()
                    path = os.path.join(dirname , 'mms*.hdf5')
                    names0 = glob.glob(path)
                    path = os.path.join(dirname , 'mms*.npz')
                    names1 = glob.glob(path)
                    names = names0 + names1
                    for name in names:
                        print("removing", name)
                        os.remove(name)

    def _local_plot_modify(self, ax):
        pass

    def _plot_convergence_rate(self, err):
        import matplotlib.pyplot as plt
        from matplotlib.ticker import MultipleLocator
        available = (
            list('bgrcmyk') +
            ['darkred', 'chocolate', 'darkorange', 'lime', 'darkgreen',
             'fuchsia', 'navy', 'indigo']
        )
        colors = {
            scheme: available[i]
            for i, scheme in enumerate(sorted(self.problems.keys()))
        }

        # If you want to do this manually
        # colors = {
        #     'WCSPH': 'k', 'EDAC': 'r', 'TVF': 'b'
        # }

        max_nx = 0
        fig, axes = plt.subplots(1, 2, sharey=True, figsize=(10, 5))
        errors = err
        for i, error in enumerate(errors):
            for re in self.re:
                dx = None
                for scheme, problem in self.problems.items():
                    cases = filter_cases(problem.cases, re=re)
                    nx, l1 = get_convergence_data(cases, nx='nx', error=error)
                    max_nx = max(nx.max(), max_nx)
                    label = self.get_label(problem)
                    hdx = problem.hdx
                    dx = 1.0/(nx)#*hdx
                    axes[i].loglog(dx, l1, color=colors[scheme],
                            linestyle='-', marker='o',
                            label=label)

                axes[i].loglog(dx, l1[0]*(dx/dx[0])**2, 'k--', linewidth=2,
                        label=r'$O(\Delta s^2)$')
                axes[i].loglog(dx, l1[0]*(dx/dx[0]), 'k:', linewidth=2,
                        label=r'$O(\Delta s)$')
                self._local_plot_modify(axes[i])
                axes[i].set_xlabel(r'$\Delta s$')
                if (error == 'p_l1') or (error == 'p_linf') or (error == 'p_l2'):
                    axes[i].text(0.85, 0.05, 'Pressure', transform=axes[i].transAxes)
                elif (error == 'l1') or (error == 'linf') or (error == 'l2'):
                    axes[i].text(0.85, 0.05, 'Velocity', transform=axes[i].transAxes)
                if i==0:
                    if errors[0] == 'p_l1':
                        axes[i].set_ylabel(r'$L_1$ error')
                    elif errors[0] == 'p_l2':
                        axes[i].set_ylabel(r'$L_2$ error')
                    else:
                        axes[i].set_ylabel(r'$L_\infty$ error')
                axes[i].grid()

        handles, labels = axes[0].get_legend_handles_labels()
        fig.legend(handles, labels, bbox_to_anchor=[0.5, 0.92], ncol=5, loc='center')
        plt.tight_layout()
        plt.subplots_adjust(top=0.83, hspace=0.0, wspace=0.0)
        plt.savefig(self.output_path(self.get_name() + '_%s_conv_re_%s.png' %(errors[1],re)))
        plt.close()


class Perturb(MMSConv):
    def get_name(self):
        return 'perturb' + self.name

    def setup(self):
        cmd = (
            ' --perturb p '
        )
        self.cases = self._make_cases(cmd)


class Unperturb(MMSConv):
    def get_name(self):
        return 'unperturb' + self.name

    def setup(self):
        cmd = (
            ' --perturb up '
        )
        self.cases = self._make_cases(cmd)


class Packed(MMSConv):
    def get_name(self):
        return 'packed' + self.name

    def setup(self):
        cmd = (
            ' --perturb pack '
        )
        self.cases = self._make_cases(cmd)


class TSPH(MMSConv):
    def get_name(self):
        return 'tsph' + self.name

    def setup(self):
        cmd = (
            ' --scheme tsph --scm wcsph --eos linear --pst-freq 10 '
        )
        self.cases = self._make_cases(cmd)


class TSPHNoCorr(MMSConv):
    def get_name(self):
        return 'tsph_no_corr' + self.name

    def setup(self):
        cmd = (
            ' --scheme tsph --scm wcsph --eos linear --pst-freq 10 --no-kernel-corr '
        )
        self.cases = self._make_cases(cmd)


class TSPHEuler(MMSConv):
    def get_name(self):
        return 'tsph_eu' + self.name

    def setup(self):
        cmd = (
            ' --scheme tsph --scm wcsph --eos linear --pst-freq 10 --intg euler '
        )
        self.cases = self._make_cases(cmd)


class MMS1(MMSConv):
    def get_name(self):
        return 'mms_1' + self.name

    def setup(self):
        cmd = (
            ' --nu 0.0 --mms mms1 --timestep 0.0000175 '
        )
        self.cases = self._make_cases(cmd)


class MMS1EDAC2(MMSConv):
    def get_name(self):
        return 'mms_1_edac_2' + self.name

    def setup(self):
        cmd = (
            ' --scheme tsph --scm edac --eos linear --pst-freq 10\
              --nu 0.0 --mms mms13 --timestep 0.0000175 --damp-pre --edac-alpha 0.5 '
        )
        self.cases = self._make_cases(cmd)


class MMS1WrongDiv(MMSConv):
    def get_name(self):
        return 'mms_1_wrong_ce' + self.name

    def setup(self):
        cmd = (
            ' --scheme tsph --scm wcsph --eos linear --pst-freq 10 --ce wrong '
        )
        self.cases = self._make_cases(cmd)

class MMS1WrongDiv1(MMSConv):
    def get_name(self):
        return 'mms_1_wrong_ce1' + self.name

    def setup(self):
        cmd = (
            ' --scheme tsph --scm wcsph --eos linear --pst-freq 10 --intg euler --ce wrong '
        )
        self.cases = self._make_cases(cmd)


class MMS1SymGrad(MMSConv):
    def get_name(self):
        return 'mms_1_sym_pe' + self.name

    def setup(self):
        cmd = (
            ' --scheme tsph --scm wcsph --eos linear --pst-freq 10 --pe sym '
        )
        self.cases = self._make_cases(cmd)


class MMS2(MMSConv):
    def get_name(self):
        return 'mms_2' + self.name

    def setup(self):
        cmd = (
            ' --nu 1. --mms mms2 --timestep 0.000000125 '
        )
        self.cases = self._make_cases(cmd)


class MMS3(MMSConv):
    def get_name(self):
        return 'mms_3' + self.name

    def setup(self):
        cmd = (
            ' --nu 0.01 --mms mms3 --timestep 0.0000175 '
        )
        self.cases = self._make_cases(cmd)


class MMS1Pie(MMSConv):
    def get_name(self):
        return 'mms_1_pie' + self.name

    def setup(self):
        cmd = (
            ' --scheme tsph_pie --scm wcsph --eos linear --pst-freq 10 '
        )
        self.cases = self._make_cases(cmd)


class MMS8(MMSConv):
    def get_name(self):
        return 'mms_8' + self.name

    def setup(self):
        cmd = (
            ' --nu 0.0 --mms mms8 --timestep 0.0000175 '
        )
        self.cases = self._make_cases(cmd)


class MMS2ClearyVisc(MMSConv):
    def get_name(self):
        return 'mms_2_cleary_ve' + self.name

    def setup(self):
        cmd = (
            ' --scheme tsph --scm wcsph --eos linear --pst-freq 10 --ve cleary '
        )
        self.cases = self._make_cases(cmd)


class MMS4(MMSConv):
    def get_name(self):
        return 'mms_4' + self.name

    def setup(self):
        cmd = (
            ' --nu 0.0 --mms mms4 --timestep 0.0000175 '
        )
        self.cases = self._make_cases(cmd)


class MMS6(MMSConv):
    def get_name(self):
        return 'mms_6' + self.name

    def setup(self):
        cmd = (
            ' --nu 1.0 --mms mms6 --timestep 0.000000125 '
        )
        self.cases = self._make_cases(cmd)


class MMS7(MMSConv):
    def get_name(self):
        return 'mms_7' + self.name

    def setup(self):
        cmd = (
            ' --nu 0.0 --mms mms7 --timestep 0.0000175 '
        )
        self.cases = self._make_cases(cmd)


class MMS8(MMSConv):
    def get_name(self):
        return 'mms_8' + self.name

    def setup(self):
        cmd = (
            ' --nu 0.0 --mms mms8 --timestep 0.0000175 '
        )
        self.cases = self._make_cases(cmd)


class MMS8DirchletVel(MMSConv):
    def get_name(self):
        return 'mms_8_dirc_vel' + self.name

    def setup(self):
        cmd = (
            ' --scheme tsph_dirc_u --scm wcsph --eos linear --pst-freq 10 '
        )
        self.cases = self._make_cases(cmd)


class MMS8DirchletP(MMSConv):
    def get_name(self):
        return 'mms_8_dirc_p' + self.name

    def setup(self):
        cmd = (
            ' --scheme tsph_dirc_p --scm wcsph --eos linear --pst-freq 10 '
        )
        self.cases = self._make_cases(cmd)


class MMS1EDAC(MMSConv):
    def get_name(self):
        return 'mms_1_edac' + self.name

    def setup(self):
        cmd = (
            ' --scheme edac --nu 0.0 --mms mms5 --edac-alpha 0.5 --timestep 0.0000175 '
        )
        self.cases = self._make_cases(cmd)


class TDSPH(MMSConv):
    def get_name(self):
        return 'tdpsh' + self.name

    def setup(self):
        cmd = (
            ' --scheme tdsph --timestep 0.0000175 '
        )
        self.cases = self._make_cases(cmd)

class MMS1EWCSPH(MMSConv):
    def get_name(self):
        return 'mms_1_ewcsph' + self.name

    def setup(self):
        cmd = (
            ' --scheme ewcsph --nu 0.0 --mms mms1 --method org --timestep 0.0000175 '
        )
        self.cases = self._make_cases(cmd)

class EWCSPHSOC(MMSConv):
    def get_name(self):
        return 'ewcsph_soc' + self.name

    def setup(self):
        cmd = (
            ' --scheme ewcsph --method soc --timestep 0.0000175 '
        )
        self.cases = self._make_cases(cmd)


class MMS4Pressure(MMSConv):
    def get_name(self):
        return 'mms_4_pressure' + self.name

    def setup(self):
        cmd = (
            ' --scheme tsph_p --scm wcsph --eos linear --pst-freq 10 '
        )
        self.cases = self._make_cases(cmd)


class MMS6NoSlip(MMSConv):
    def get_name(self):
        return 'mms_6_no_slip' + self.name

    def setup(self):
        cmd = (
            ' --scheme tsph_uns --scm wcsph --eos linear --pst-freq 10 '
        )
        self.cases = self._make_cases(cmd)


class MMS7Slip(MMSConv):
    def get_name(self):
        return 'mms_7_slip' + self.name

    def setup(self):
        cmd = (
            ' --scheme tsph_us --scm wcsph --eos linear --pst-freq 10 '
        )
        self.cases = self._make_cases(cmd)


class HDX1_4(MMSConv):
    def get_name(self):
        return 'hdx1_4' + self.name

    def setup(self):
        cmd = (
            ' --scheme tsph --scm wcsph --eos linear --pst-freq 10 '
        )
        self.cases = self._make_cases(cmd, hdx_override=1.4)


comp1 = [MMS1, MMS3, MMS4, MMS6, MMS7, MMS8]
comp_mesh = [Perturb, Unperturb, Packed]
compare_edac = [MMS1EDAC, MMS1EDAC2]
var_scheme = [EWCSPHSOC, TDSPH]
comp_shape = [TSPH, MMS1Pie]

comp_wrong_ce = [TSPHEuler, MMS1WrongDiv, MMS1WrongDiv1]
comp_wrong_grad = [TSPH, MMS1SymGrad]
comp_wrong_ve = [TSPH, MMS2ClearyVisc]

dirch_bc = [MMS8DirchletP, MMS8DirchletVel, TSPH]
pressure_bc = [MMS4Pressure, TSPH]
no_slip_bc = [MMS6NoSlip, TSPH]
slip_bc = [MMS7Slip, TSPH]

var_domain = [TSPH, HDX1_4]
var_domain_freq = [TSPH, HDX1_4]
var_domain_freq_10 = [TSPH, HDX1_4]
var_domain_3d = [TSPH, TSPHNoCorr]

# -------------------------------------------------------------------------
# Comparison of the different schemes.
class MMSCompareEDAC(Comparison):
    def get_name(self):
        return 'mms_compare_edac'

    def get_label(self, problem):
        params = problem.cases[0].command.split()

        if 'tsph' in params:
            return 'PE-IPST-C'
        else:
            return 'EDAC'

    def setup(self):
        cmd = ' --max-step 1 '
        self._make_case(compare_edac, tf=0.1, common_cmd=cmd)
        self._set_re('MMS1EDAC')


class MMSCompareEDAC100(Comparison):
    def get_name(self):
        return 'mms_compare_edac_100'

    def get_label(self, problem):
        params = problem.cases[0].command.split()

        if 'tsph' in params:
            return 'PE-IPST-C'
        else:
            return 'EDAC'

    def setup(self):
        cmd = ' --max-step 100 '
        self._make_case(compare_edac, tf=0.1, common_cmd=cmd, name='_100s')
        self._set_re('MMS1EDAC')

class MMSCompare(Comparison):
    def get_name(self):
        return 'mms_compare'

    def get_label(self, problem):
        params = problem.cases[0].command.split()

        if 'mms1' in params:
            return 'MMS1'
        elif 'mms2' in params:
            return 'MMS2'
        elif 'mms3' in params:
            return 'MMS3'
        elif 'mms4' in params:
            return 'MMS4'
        elif 'mms6' in params:
            return 'MMS6'
        elif 'mms7' in params:
            return 'MMS7'
        elif 'mms8' in params:
            return 'MMS8'


    def setup(self):
        cmd = ' --scheme tsph --scm wcsph --eos linear --pst-freq 10 --max-step 100 --perturb pack '
        # cmd = ' --max-step 200 '
        self._make_case(comp1, tf=0.1, common_cmd=cmd, name='_lone',  err=['p_linf', 'linf'])
        self._set_re('MMS3')

    def run(self):
        super(MMSCompare, self).run()

        self._plot_convergence_rate(err=['p_l1', 'l1'])
        self._plot_convergence_rate(err=['p_l2', 'l2'])


class MMSCompareShape(Comparison):
    def get_name(self):
        return 'mms_compare_shape'

    def get_label(self, problem):
        params = problem.cases[0].command.split()

        if 'tsph' in params:
            return 'square'
        else:
            return 'butterfly'

    def setup(self):
        cmd = ' --max-step 100 --nu 0.01 --mms mms11 --timestep 0.0000175  '
        self._make_case(comp_shape, tf=0.1, common_cmd=cmd, name='_var', err=['p_linf', 'linf'])
        self._set_re('TSPH')

    def run(self):
        super(MMSCompareShape, self).run()

        self._plot_convergence_rate(err=['p_l1', 'l1'])
        self._plot_convergence_rate(err=['p_l2', 'l2'])


class MMSCompareVariations(Comparison):
    def get_name(self):
        return 'mms_compare_variations'

    def get_label(self, problem):
        params = problem.cases[0].command.split()

        if 'ewcsph' in params:
            return 'E-C'
        elif 'tdsph' in params:
            return 'TV-C'

    def setup(self):
        cmd = ' --max-step 100 --mms mms1 --nu 0.0 '
        self._make_case(var_scheme, tf=0.1, common_cmd=cmd)
        self._set_re('EWCSPHSOC')


class MMSCompareWrong(Comparison):

    def get_label(self, problem):
        params = problem.cases[0].command.split()

        if 'sym' in params:
            return "sym"
        elif 'cleary' in params:
            return 'Cleary'
        elif 'wrong' in params:
            if 'euler' in params:
                return 'incorrect CE-Euler'
            else:
                return 'incorrect CE-RK2'
        elif 'mms7' in params:
            return 'L-IPST-C'
        elif 'mms2' in params:
            return 'L-IPST-C'


class MMSCompareWrongCE(MMSCompareWrong):
    def get_name(self):
        return 'mms_compare_wrong_ce'

    def setup(self):
        # euler not added here since mms1 should be rk2 for comp
        cmd = ' --max-step 1 --perturb pack --nu 0.0 --mms mms7  --timestep 0.0000075 '
        self._make_case(comp_wrong_ce, tf=0.1, common_cmd=cmd, name='_ce')
        self._set_re('TSPHEuler')


class MMSCompareWrongPE(MMSCompareWrong):
    def get_name(self):
        return 'mms_compare_wrong_pe'

    def setup(self):
        cmd = ' --intg euler --max-step 1 --perturb pack --nu 0.0 --mms mms7  --timestep 0.0000075 '
        self._make_case(comp_wrong_grad, tf=0.1, common_cmd=cmd, name='_pe')
        self._set_re('TSPH')


class MMSCompareWrongVE(MMSCompareWrong):
    def get_name(self):
        return 'mms_compare_wrong_ve'

    def setup(self):
        cmd = ' --intg euler --max-step 1 --timestep  0.0000005  --perturb pack --nu .25 --mms mms2 '
        self._make_case(comp_wrong_ve, tf=0.1, common_cmd=cmd, name='_ve')
        self._set_re('TSPH')



class DirichletBCCompare(Comparison):
    def get_name(self):
        return 'dirichlet_bc'

    def get_label(self, problem):
        params = problem.cases[0].command.split()

        if 'tsph' in params:
            return "MS"
        elif 'tsph_dirc_u' in params:
            return "velocity BC"
        elif 'tsph_dirc_p' in params:
            return "pressure BC"


    def setup(self):
        cmd = ' --max-step 100 --nu 0.0 --mms mms8 --timestep 0.0000175 '
        self._make_case(dirch_bc, tf=0.1, common_cmd=cmd, err=['p_linf', 'linf'], name='_dir')
        self._set_re('TSPH')


class PressureBCCompare(Comparison):
    def get_name(self):
        return 'pressure_bc'

    def get_label(self, problem):
        params = problem.cases[0].command.split()

        if 'tsph' in params:
            return "MS"
        elif 'tsph_p' in params:
            return "Neumann BC"


    def setup(self):
        cmd = ' --max-step 100 --nu 0.0 --mms mms4 --timestep 0.0000175 '
        self._make_case(pressure_bc, tf=0.1, common_cmd=cmd, err=['p_linf', 'linf'], name='_pr')
        self._set_re('TSPH')


class NoSlipBCCompare(Comparison):
    def get_name(self):
        return 'no_slip'

    def get_label(self, problem):
        params = problem.cases[0].command.split()

        if 'tsph' in params:
            return "MS"
        elif 'tsph_uns' in params:
            return "no-slip BC"


    def setup(self):
        cmd = ' --max-step 100 --nu 1.0 --mms mms6 --timestep 0.000000125 '
        self._make_case(no_slip_bc, tf=0.1, common_cmd=cmd, err=['p_linf', 'linf'], name='_ns')
        self._set_re('TSPH')


class SlipBCCompare(Comparison):
    def get_name(self):
        return 'slip'

    def get_label(self, problem):
        params = problem.cases[0].command.split()

        if 'tsph' in params:
            return "MS"
        elif 'tsph_us' in params:
            return "slip BC"

    def setup(self):
        cmd = ' --max-step 100 --nu 0.0 --mms mms7 --timestep 0.0000175 '
        self._make_case(slip_bc, tf=0.1, common_cmd=cmd, err=['p_linf', 'linf'], name='_slip')
        self._set_re('TSPH')

    def run(self):
        super(SlipBCCompare, self).run()

        self._plot_convergence_rate(err=['p_l1', 'l1'])
        self._plot_convergence_rate(err=['p_l2', 'l2'])


class CompareInitialParticles(Comparison):
    def get_name(self):
        return 'particle_dist'

    def get_label(self, problem):
        params = problem.cases[0].command.split()

        if 'pack' in params:
            return 'Pack'
        elif 'up' in params:
            return 'Unperturb'
        elif 'p' in params:
            return 'Perturb'

    def setup(self):
        cmd = ' --scheme tsph --scm wcsph --eos linear --pst-freq 10 \
            --nu 0.01 --mms mms3 --timestep 0.0000175 --max-step 100 '
        self._make_case(comp_mesh, tf=0.1, common_cmd=cmd)
        self._set_re('Perturb')


class CompareInitialParticles10(CompareInitialParticles):
    def get_name(self):
        return 'particle_dist_10'

    def setup(self):
        cmd = ' --scheme tsph --scm wcsph --eos linear --pst-freq 10 \
            --nu 0.01 --mms mms3 --timestep 0.0000175 --max-step 10 '
        self._make_case(comp_mesh, tf=0.1, common_cmd=cmd, name='_10')
        self._set_re('Perturb')


class CompareInitialParticlesDiv(CompareInitialParticles):
    def get_name(self):
        return 'particle_dist_div'

    def setup(self):
        cmd = ' --scheme tsph --scm wcsph --eos linear --pst-freq 10 --nu 0.01 \
            --mms mms11 --timestep 0.0000175 --max-step 10 '
        self._make_case(comp_mesh, tf=0.1, common_cmd=cmd, name='_div')
        self._set_re('Perturb')


class VarDomainCompare(Comparison):
    def get_name(self):
        return 'var_domain'

    def get_label(self, problem):
        hdx = problem.cases[0].params['hdx']
        if hdx == 1.2:
            return r"$h_{\Delta s}=1.2$"
        else:
            return r"$h_{\Delta s}=1.4$"

    def setup(self):
        cmd = ' --max-step 100 --scheme tsph --scm wcsph --eos linear --pst-freq 10 \
            --nu 0.01 --mms mms11 --timestep 0.000000195 --area var '
        self._make_case(var_domain, tf=0.1, common_cmd=cmd, res=RESOLUTIONS2,
                        err=['p_linf', 'linf'], name='var1')
        self._set_re('TSPH')


class VarDomainCompare3D(Comparison):
    def get_name(self):
        return 'var_domain_3d'

    def _local_plot_modify(self, ax):
        ax.xaxis.set_minor_locator(plt.MaxNLocator(4))
        ax.grid(which='both', axis='x')

    def get_label(self, problem):
        params = problem.cases[0].command.split()

        if '--no-kernel-corr' in params:
            return r"No kernel correction"
        else:
            return r"With kernel correction"

    def setup(self):
        cmd = ' --max-step 10 --scheme tsph --scm wcsph --eos linear --pst-freq 10 \
            --nu 0.01 --mms mms12 --timestep 0.00001875 --area var --dim 3 '
        self._make_case(var_domain_3d, tf=0.1, common_cmd=cmd, res=[100, 200, 400, 800],
                        err=['p_linf', 'linf'], name='var_3d')
        self._set_re('TSPH')


class VarDomainFreqCompare(Comparison):
    def get_name(self):
        return 'var_domain_freq'

    def get_label(self, problem):
        hdx = problem.cases[0].params['hdx']
        if hdx == 1.2:
            return r"$h_{\Delta s}=1.2$"
        else:
            return r"$h_{\Delta s}=1.4$"

    def setup(self):
        cmd = ' --max-step 100 --scheme tsph --scm wcsph --eos linear --pst-freq 10 \
             --nu 0.01 --mms mms9 --timestep 0.000000195 --area var '

        self._make_case(var_domain, tf=0.1, common_cmd=cmd, res=RESOLUTIONS2,
                        err=['p_linf', 'linf'], name='var_fre')
        self._set_re('TSPH')


class VarDomainFreqCompareNoCorrectionMMS6(Comparison):
    def get_name(self):
        return 'var_domain_mms6_no_corr'

    def get_label(self, problem):
        hdx = problem.cases[0].params['hdx']
        if hdx == 1.2:
            return r"$h_{\Delta s}=1.2$"
        else:
            return r"$h_{\Delta s}=1.4$"

    def setup(self):
        cmd = ' --max-step 100 --scheme tsph --scm wcsph --eos linear --pst-freq 10 \
             --nu .01 --mms mms11 --timestep 0.000000195 --area var --no-kernel-corr '

        self._make_case(var_domain, tf=0.1, common_cmd=cmd, res=RESOLUTIONS2,
                        err=['p_linf', 'linf'], name='_6_no_corr')
        self._set_re('TSPH')


class VarDomainFreqCompareNoKernelCorrection(Comparison):
    def get_name(self):
        return 'var_domain_no_corr'

    def get_label(self, problem):
        hdx = problem.cases[0].params['hdx']
        if hdx == 1.2:
            return r"$h_{\Delta s}=1.2$"
        else:
            return r"$h_{\Delta s}=1.4$"

    def setup(self):
        cmd = ' --max-step 100 --scheme tsph --scm wcsph --eos linear --pst-freq 10 \
             --nu .01 --mms mms9 --timestep 0.000000195 --area var --no-kernel-corr '

        self._make_case(var_domain, tf=0.1, common_cmd=cmd, res=RESOLUTIONS2,
                        err=['p_linf', 'linf'], name='_no_kc')
        self._set_re('TSPH')


class TaylorGreen(PySPHProblem):
    def get_name(self):
        return 'tg'

    def setup(self):
        self.nx = [50, 100, 200]

        _cmd = ' python code/taylor_green.py --openmp --pe sym --pst-freq 10 '
        self.cases = [
            Simulation(
                self.input_path('nx_%d' % (nx)), _cmd,
                job_info=dict(
                    n_core=NCORE, n_thread=NTHREAD), nx=nx,
                    timestep=0.000113636, pfreq=1000, tf=1.0
            )
            for nx in self.nx
        ]

    def run(self):
        self.make_output_dir()
        self._plot_decay()
        self._plot_l1_error()

    def _plot_decay(self):
        from matplotlib.ticker import FormatStrFormatter
        time, d_a = None, None
        for case in self.cases:
            data = case.data
            nx = case.params['nx']

            time = data['t']
            decay = data['decay']
            d_a = data['decay_ex']

            plt.semilogy(time, decay, label='N=%d'%nx)

        plt.semilogy(time, d_a, '--k', label='Exact')
        plt.xlabel('t')
        plt.ylabel(r'$|\mathbf{u}|$')
        ax = plt.gca()
        ax.yaxis.set_minor_formatter(FormatStrFormatter('%.1f'))
        plt.legend()
        plt.grid()
        fig = os.path.join(self.output_path(), "decay.png")
        plt.savefig(fig, dpi=300)
        plt.close()

    def _plot_l1_error(self):
        nx, l1 = get_convergence_data(self.cases, nx='nx', error='l1')
        dx = 1/nx
        plt.plot(dx, l1, label='Error')
        plt.loglog(dx, l1[0]*(dx/dx[0])**2, 'k--', linewidth=2,
                label=r'$O(\Delta s^2)$')
        plt.loglog(dx, l1[0]*(dx/dx[0]), 'k:', linewidth=2,
                label=r'$O(\Delta s)$')

        plt.ylabel(r'$L_1$')
        plt.xlabel(r'$\Delta s$')

        plt.legend()
        plt.grid()
        fig = os.path.join(self.output_path(), "l1.png")
        plt.savefig(fig, dpi=300)
        plt.close()


class Cavity(PySPHProblem):
    def get_name(self):
        return 'ldc'

    def setup(self):
        self.nx = [50, 100, 200]

        _cmd = ' python code/lid_driven_cavity.py --openmp --ve cleary --pst-freq 10 '
        self.cases = [
            Simulation(
                self.input_path('nx_%d' % (nx)), _cmd,
                job_info=dict(
                    n_core=NCORE, n_thread=NTHREAD), nx=nx, timestep=0.000113636, pfreq=5000
            )
            for nx in self.nx
        ]

    def run(self):
        self.make_output_dir()
        self._plot_velocities()

    def _plot_velocities(self):
        from pysph.examples.ghia_cavity_data import get_u_vs_y, get_v_vs_x

        s1 = plt.subplot(211)
        s2 = plt.subplot(212)

        for case in self.cases:
            data = case.data
            nx = case.params['nx']

            _x = data['x']
            ui_c = data['u_c']
            vi_c = data['v_c']

            s1.plot(ui_c, _x, label='N=%d'%nx)
            s2.plot(_x, vi_c, label='N=%d'%nx)

        y, data = get_u_vs_y()
        s1.plot(data[100], y, 'o', fillstyle='none',
                label='Ghia et al.')
        s1.set_xlabel(r'$v_x$')
        s1.set_ylabel(r'$x$')
        s1.grid()
        s1.legend()

        x, data = get_v_vs_x()
        s2.plot(x, data[100], 'o', fillstyle='none',
                label='Ghia et al.')
        s2.set_xlabel(r'$x$')
        s2.set_ylabel(r'$v_y$')
        s2.grid()
        s2.legend()

        fig = os.path.join(self.output_path(), 'centerline.png')
        plt.tight_layout()
        plt.savefig(fig, dpi=300)
        plt.close()


def create_particle_files(plotfile):
    import matplotlib.pyplot as plt
    filename = os.path.join('code', 'mesh', 'nx50.npz')
    data_pack = np.load(filename)
    x_pack, y_pack = data_pack['x'] + 0.5, data_pack['y'] + 0.5

    dx = 1./50
    x, y = np.mgrid[dx/2:1:dx, dx/2:1:dx]
    x, y = [np.ravel(t) for t in (x, y)]

    np.random.seed(10)
    xp = x + (np.random.random(len(x)) - 0.5)/0.5 * dx * 0.1
    yp = y + (np.random.random(len(x)) - 0.5)/0.5 * dx * 0.1

    nl = 6 * dx
    xs, ys = np.mgrid[dx/2-nl:1+nl:dx, dx/2-nl:1+nl:dx]
    cond = ~((xs > 0) & (xs < 1) & (ys > 0) & (ys < 1))
    xs, ys = xs[cond], ys[cond]

    s = 6.0
    fig, axes = plt.subplots(1, 3, sharey=True, figsize=(12, 4))
    axes[0].scatter(x, y, s=s)
    axes[0].scatter(xs, ys, s=s)
    axes[0].set_title('Unperturbed')
    axes[1].scatter(xp, yp, s=s)
    axes[1].scatter(xs, ys, s=s)
    axes[1].set_title('Perturbed')
    axes[2].scatter(x_pack, y_pack, s=s)
    axes[2].scatter(xs, ys, s=s)
    axes[2].set_title('Packed')
    plt.tight_layout(pad=0.1)
    plt.savefig(plotfile, dpi=300)
    plt.close()

def create_particle_files2(plotfile):
    import matplotlib.pyplot as plt
    dx = 1.0/50

    x, y = np.mgrid[dx/2:1:dx, dx/2:1:dx]
    x, y = [np.ravel(t) for t in (x, y)]

    nl = 6 * dx
    xs, ys = np.mgrid[dx/2-nl:1+nl:dx, dx/2-nl:1+nl:dx]
    cond = ~((xs > 0) & (xs < 1) & (ys > 0) & (ys < 1))
    cond1 = cond & (ys>1.0)
    cond2 = cond & (ys<1.0)
    xs1, ys1 = xs[cond1], ys[cond1]
    xs2, ys2 = xs[cond2], ys[cond2]

    s = 6.0
    plt.scatter(x, y, s=s)
    plt.scatter(xs1, ys1, s=s)
    plt.scatter(xs2, ys2, s=s)
    plt.tight_layout(pad=0.1)
    plt.savefig(plotfile, dpi=300)
    plt.close()

def create_particle_files3(plotfile):
    import matplotlib.pyplot as plt

    dx = 1./50
    x, y = np.mgrid[dx/2:1:dx, dx/2:1:dx]
    x, y = [np.ravel(t) for t in (x, y)]

    nl = 6 * dx
    xs, ys = np.mgrid[dx/2-nl:1+nl:dx, dx/2-nl:1+nl:dx]
    cond = ~((xs > 0) & (xs < 1) & (ys > 0) & (ys < 1))
    xs_, ys_ = xs[cond], ys[cond]
    theta = np.arctan2(ys-0.5, xs-0.5)

    # r = 0.5
    r = 0.1*(3 + np.cos(4 * theta) + np.sin(2 * theta))
    cond = (xs-0.5)**2 + (ys-0.5)**2 - r**2 < 1e-14
    fx, fy = xs[cond], ys[cond]

    # r = 0.5 + nl
    r = (0.1)*(3 + np.cos( 4 * theta) + np.sin(2 * theta)) +nl
    cond = (xs-0.5)**2 + (ys-0.5)**2 - r**2 < 1e-14
    sx, sy = xs[cond], ys[cond]

    # Initialize
    from pysph.tools.geometry import remove_overlap_particles
    from pysph.base.utils import get_particle_array
    fluid = get_particle_array(name='fluid', x=fx, y=fy, h=dx)
    solid = get_particle_array(name='solid', x=sx, y=sy, h=dx)
    remove_overlap_particles(solid, fluid, dx, dim=2)

    s = 6.0
    fig, axes = plt.subplots(1, 2, sharey=True, figsize=(8, 4))
    axes[0].scatter(x, y, s=s)
    axes[0].scatter(xs_, ys_, s=s)
    axes[0].set_title('Square')
    axes[1].scatter(fx, fy, s=s)
    axes[1].scatter(solid.x, solid.y, s=s)
    axes[1].set_title('Butterfly')
    plt.tight_layout(pad=0.1)
    plt.savefig(plotfile, dpi=300)
    plt.close()

def create_particle_files4(plotfile):
    import matplotlib.pyplot as plt
    from matplotlib.patches import Rectangle
    dx = 1.0/50

    x, y = np.mgrid[dx/2:1:dx, dx/2:1:dx]
    x, y = [np.ravel(t) for t in (x, y)]

    nl = 6 * dx
    xs, ys = np.mgrid[dx/2-nl:1+nl:dx, dx/2-nl:1+nl:dx]
    cond = ~((xs > 0) & (xs < 1) & (ys > 0) & (ys < 1))
    xs, ys = xs[cond], ys[cond]

    s = 6.0
    plt.scatter(x, y, s=s)
    plt.scatter(xs, ys, s=s)
    ax = plt.gca()
    ax.add_patch(
        Rectangle((0.4375, 0.4375), 0.125, 0.125, fc ='none', ec ='r', lw = 1)
    )
    ax.add_patch(
        Rectangle((0.45, 0.45), 0.1, 0.1, fc ='none', ec ='k', lw = 1)
    )
    plt.tight_layout(pad=0.1)
    plt.savefig(plotfile, dpi=300)
    plt.close()


if __name__ == '__main__':
    from code.mms_creator import create_mms
    create_mms()
    PROBLEMS = [
        VarDomainCompare, VarDomainFreqCompare, MMSCompare, MMSCompareWrongCE,
        MMSCompareWrongPE, MMSCompareWrongVE, CompareInitialParticles,
        CompareInitialParticles10, PressureBCCompare, NoSlipBCCompare,
        SlipBCCompare, MMSCompareEDAC, MMSCompareEDAC100, MMSCompareVariations,
        DirichletBCCompare, MMSCompareShape, CompareInitialParticlesDiv,
        VarDomainFreqCompareNoKernelCorrection,
        VarDomainFreqCompareNoCorrectionMMS6, VarDomainCompare3D,
        TaylorGreen, Cavity
    ]

    automator = Automator(
        simulation_dir='outputs',
        output_dir=os.path.join('manuscript', 'figures'),
        all_problems=PROBLEMS
    )
    automator.run()

    plotfile = os.path.join('manuscript', 'figures', 'particles.png')
    create_particle_files(plotfile)
    plotfile = os.path.join('manuscript', 'figures', 'particles2.png')
    create_particle_files2(plotfile)
    plotfile = os.path.join('manuscript', 'figures', 'particles3.png')
    create_particle_files3(plotfile)
    plotfile = os.path.join('manuscript', 'figures', 'particles4.png')
    create_particle_files4(plotfile)